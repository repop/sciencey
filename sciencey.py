adj = ["quantum", "psycological",
"temporal", "ghost", "controlling",
"control", "central", "datum",
"variable", "dependent", "double",
"empty", "experimental", "random",
"null", "internal", "gravitational"]
noun = ["foam", "time", "machine",
"field", "laser", "matrix",
"interaction", "hypothesis",
"experiment", "theorem", "limit",
"randomness", "levels", "permutation",
"fusion", "gravity", "at distance"]

from sys import argv
from random import choice
def new_bullshit(amount):
    lastnoun = choice(noun) 
    text = ""
    for n in range(amount):
        if n != 0:
          nowList = choice([noun, adj])
          nowWord = choice(nowList)
          text += nowWord + " "
    text += lastnoun
    print(text)

if len(argv) < 2:
    print("put how many words you wanna generate")
elif len(argv) > 3:
    print("more than word amount and phrase amount is needless")
elif len(argv) == 2:
    new_bullshit(int(argv[1]))
elif len(argv) == 3:
    for n in range(int(argv[2])):
        new_bullshit(int(argv[1]))
